from kafka import SimpleProducer, SimpleClient

host = "0.0.0.0"
port = int(sys.argv[1])
aggSize = int(sys.argv[2])

class client(Thread):
    def __init__(self, producer, address, aggSize, barrier, type):
        Thread.__init__(self)
        self.addr = address
        self.aggSize = aggSize
        self.barrier = barrier
        self.type = type
        self.producer = producer
        self.start()

    def run(self):
        print("Running with {}".format(self.aggSize) + " and type {}".format(self.type))
        start = time.time()
        pathdir = '/home/ubuntu/python/twitter'
        #pathdir = 'C:\\Users\\Paulo\\Documents\\dataset'
        for filename in os.listdir(pathdir):
            with open(pathdir + '/' +filename, 'rb') as fp:
            	for line in fp:
            		producer.send_messages('app1', b'%d' % line)
        producer.stop()
        end = time.time()
        print(current_thread().name,
          'waiting for barrier with {} others'.format(
              self.barrier.n_waiting))
        self.barrier.wait()
        print ("Time: {} ".format(end - start) + "Agg: {} ".format(self.aggSize))

# To send messages asynchronously
client = SimpleClient(host, port)

# To send messages in batch. You can use any of the available
# producers for doing this. The following producer will collect
# messages in batch and send them to Kafka after 20 messages are
# collected or every 60 seconds
# Notes:
# * If the producer dies before the messages are sent, there will be losses
# * Call producer.stop() to send the messages and cleanup
producer = SimpleProducer(client,
                          async=True,
                          batch_send_every_n=aggSize)

NUM_THREADS = 10
barrier = Barrier(NUM_THREADS)
for i in range(0, NUM_THREADS):
    client(producer, address, aggSize, barrier, type)